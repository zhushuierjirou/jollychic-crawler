# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


import pymysql
import json


class JollychicPipeline(object):
    connection = pymysql.connect(host='localhost', user='root', password='openql',
                                 db='jollychic', charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    def str2int(self, string, default=None):
        try:
            i = int(string)
            return i
        except:
            return default

    def str2float(self, string):
        try:
            i = float(string)
            return i
        except:
            return None

    def handleprice(self, string):
        try:
            if string.find('-') != -1:
                arr = string.split('-')
                p1 = float(arr[0])
                p2 = float(arr[1])
                return str((p1 + p2) / 2)
            return string
        except:
            return string

    def process_item(self, item, spider):
        try:
            with self.connection.cursor() as cursor:
                sql1 = "SELECT * FROM items WHERE id1=%s"
                cursor.execute(sql1, (item['id1']))
                result = cursor.fetchone()
                if result is None:
                    sql2 = "INSERT INTO jollychic.items (name, id1, id2, comment, favorite, price, original_price, url, `cat1`, `cat2`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s. %s, %s)"
                    cursor.execute(sql2, (item['name'], self.str2int(item['id1']), item['id2']
                                          , self.str2int(item['comment'], 0), self.str2int(item['favorite'], 0)
                                          , self.str2float(self.handleprice(item['price'])),
                                          self.str2float(item['original_price']), item['url'], item['cat1'], item['cat2']))
                    self.connection.commit()
                else:
                    sql3 = "UPDATE jollychic.items SET name = %s, comment = %s, favorite = %s, price = %s, original_price = %s, updated_at = CURRENT_TIMESTAMP," \
                           " url = %s, cat1 = `%s`, cat2 = `%s`" \
                           "WHERE id1 = %s"
                    cursor.execute(sql3,
                                    (item['name'], self.str2int(item['comment'], 0), self.str2int(item['favorite'], 0),
                                    self.str2float(self.handleprice(item['price'])),
                                    self.str2float(item['original_price']), item['url'], item['cat1'], item['cat2'], item['id1']))
                    self.connection.commit()
        except Exception as e:
            print e
        finally:
            return item