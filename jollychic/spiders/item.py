# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
import re


class ItemSpider(CrawlSpider):
    name = 'item'
    allowed_domains = ['jollychic.com']
    # start_urls = ['http://jollychic.com/womens-clothing-c2']
    start_urls = [
        "http://jollychic.com/womens-clothing-c2",
        "http://jollychic.com/womens-shoes-c59",
        "http://jollychic.com/womens-bags-c35",
        "http://jollychic.com/womens-accessories-c31",
        "http://jollychic.com/mens-clothing-c324",
        "http://jollychic.com/mens-shoes-c364",
        "http://jollychic.com/mens-bags-c388",
        "http://jollychic.com/beauty-c284",
        "http://jollychic.com/kids-c179",
        "http://jollychic.com/toys-c189",
        "http://jollychic.com/baby-moms-c171",
        "http://jollychic.com/stationery-c257",
        "http://jollychic.com/cell-phones-digital-c400",
        "http://jollychic.com/appliances-c416",
        "http://jollychic.com/car-electronics-c426",
        "http://jollychic.com/dining-storage-c155",
        "http://jollychic.com/bed-bath-c149",
        "http://jollychic.com/home-decor-c131",
        "http://jollychic.com/home-essentials-c458",
    ]

    rules = (
        Rule(LinkExtractor(allow=r'/p/'), callback='parse_item'),
        Rule(LinkExtractor(allow=r'http://www.jollychic.com/.*?c\d+\?jsort')),
    )

    def parse_item(self, response):
        i = {}
        i['id1'] = response.xpath("//input[@name='goodsId']/@value").extract_first()
        i['id2'] = response.xpath("//div[@class='goods-tlt']//em/text()").extract_first()
        i['name'] = response.xpath("//div[@class='goods-tlt']//span/text()").extract_first()
        i['original_price'] = response.xpath("//*[@id='J-discount-price']/span/text()").extract_first()
        i['price'] = "".join(response.xpath("//*[@id='J-sku-price']/span//text()").extract())
        i['comment'] = re.search('"totalCount":(\d+)', response.body_as_unicode()).group(1)
        i['favorite'] = response.xpath("//*[contains(@class, 'J-addWish-num')]/text()").extract_first()
        cats = response.xpath("//div[contains(@class, 'nav_tag')]/div//h2/text()").extract()
        i['cat1'] = cats[0]
        if len(cats) >= 3:
            i['cat2'] = cats[1]
        i['url'] = response.url
        return i
