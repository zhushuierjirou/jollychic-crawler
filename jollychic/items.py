# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class JollychicItem(scrapy.Item):
    name = scrapy.Field()
    id1 = scrapy.Field()
    id2 = scrapy.Field()
    original_price = scrapy.Field()
    price = scrapy.Field()
    comment = scrapy.Field()
    favorite = scrapy.Field()
    url = scrapy.Field()
    cat1 = scrapy.Field()
    cat2 = scrapy.Field()
