import json
import pymysql

connection = pymysql.connect(host='localhost', user='root', password='openql', db='jollychic', charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

def str2int(string, default = None):
    try:
        i = int(string)
        return i
    except:
        return default

def str2float(string):
    try:
        i = float(string)
        return i
    except:
        return None

def handleprice(string):
    try:
        if string.find('-') != -1:
            arr = string.split('-')
            p1 = float(arr[0])
            p2 = float(arr[1])
            return str((p1 + p2) / 2)
        return string
    except:
        return string

try:
    with connection.cursor() as cursor:
        sql = "INSERT INTO jollychic.items (name, id1, id2, comment, favorite, price, original_price) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        with open('items.txt') as file:
            for line in file.readlines():
                line = line.strip('\n')
                item_json = json.loads(line)
                cursor.execute(sql, (item_json['name'], str2int(item_json['id1']), item_json['id2']
                                     , str2int(item_json['comment']), str2int(item_json['favorite'], 0)
                                     , str2float(handleprice(item_json['price'])), str2float(item_json['original_price'])))
    connection.commit()
finally:
    connection.close()
